# Direction Annotator

Eight-way direction annotator/viewer for pedestrian images cropped from MOT20 trainset.

# Quickstart
Run `python webapp.py`, then open `http://localhost:5000` in your browser. Requires Python3 and Flask.

Annotated data is available at `./data/directions.csv`.

Full annotation manual can be found [here](https://drive.google.com/file/d/1x5QIRba6uBKL9um54yRXiwaNGnCqQavO/view?usp=sharing).

## `crop.py`
Run on MOT20 trainset folder. Crops the four training sequences based on their bounding boxes and keeps around 8000 images. Original MOT20 annotations for the bounding boxes selected are copied to `cropped_data.csv`.

Join `data/directions.csv` and `cropped_data.csv` to get full annotations.
