import os
import pandas as pd

from PIL import Image

seqs = ["MOT20-01", "MOT20-02", "MOT20-03", "MOT20-05"]
cols = ["frame", "track_id", "left", "top", "width", "height", "confidence", "class", "visibility"]

n = 8000

if __name__ == "__main__":
    if not os.path.exists('cropped'):
        os.makedirs('cropped')

    df_list = []
    for i, seq in enumerate(seqs):
        df = pd.read_csv(os.sep.join([seq, "gt", "gt.txt"]), names=cols)
        df.insert(0, 'line', df.index)
        df.insert(0, 'seq', i)
        df_list.append(df)

    df = pd.concat(df_list)

    # Filter pedestrians and still persons
    df = df[(df["class"] == 1) | (df["class"] == 7)]

    # Get n rows (boxes)
    df = df[::(len(df) // n)]

    for row in df.itertuples():
        im = Image.open(os.sep.join([seqs[row.seq], "img1", f"{row.frame:06}.jpg"]))
        im_crop = im.crop((row.left, row.top, row.left + row.width, row.top + row.height))
        im_crop.save(os.sep.join(["cropped", f"{seqs[row.seq]}_{row.line:07}.png"]))

    df.to_csv("cropped_data.csv", index=False)
