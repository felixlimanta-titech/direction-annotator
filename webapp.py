import json
import glob
import os
import pandas as pd

from flask import Flask, render_template, request, send_from_directory

data_path = "./data"
csv_path = "./data/directions.csv"

app = Flask(__name__)

@app.route("/")
def index():
    # Set annotated dir, conf if available
    if curr_processed in df.index:
        dir, conf = (int(df.loc[curr_processed]["direction"]), int(df.loc[curr_processed]["confidence"]))
    else:
        dir, conf = 0, 0

    return render_template('index.html', fname=os.sep.join([data_path, curr_processed]),
                           dir=dir, conf=conf, total_count=len(file_list), curr_count=(curr_processed_i + 1))

@app.route("/data/<path:filename>")
def serve_data_img(filename):
    return send_from_directory(app.root_path + "/data/", filename)

@app.route("/set", methods=['POST'])
def set_direction():
    global curr_processed_i, curr_processed

    data = request.get_json()

    if data["dir"] == "-1":
        data["conf"] = "1"

    # Save direction to CSV
    df.loc[curr_processed] = [data["dir"], data["conf"]]
    df.to_csv(csv_path)

    # Load next file
    curr_processed = file_list[curr_processed_i + 1]
    curr_processed_i += 1

    return json.dumps({
        "fname": os.sep.join([data_path, curr_processed]),
        "dir": int(df.loc[curr_processed]["direction"]) if curr_processed in df.index else 0,
        "conf": int(df.loc[curr_processed]["confidence"]) if curr_processed in df.index else 0,
        "curr_count": curr_processed_i + 1
    })

@app.route("/nav")
def get_prev_next_img():
    global curr_processed_i, curr_processed

    curr_processed = file_list[curr_processed_i + int(request.args.get("nav"))]
    curr_processed_i += int(request.args.get("nav"))

    return json.dumps({
        "fname": os.sep.join([data_path, curr_processed]),
        "dir": int(df.loc[curr_processed]["direction"]) if curr_processed in df.index else 0,
        "conf": int(df.loc[curr_processed]["confidence"]) if curr_processed in df.index else 0,
        "curr_count": curr_processed_i + 1
    })

if __name__ == "__main__":
    # Check CSV, open last processed file + 1
    file_list = [os.path.basename(x) for x in sorted(glob.glob(os.sep.join([data_path, '*.png'])))]
    curr_processed_i = 0

    if os.path.exists(csv_path):
        df = pd.read_csv(csv_path)
        df = df.set_index('filename')

        last_processed = df.tail(1).index[0]
        last_processed_i = file_list.index(last_processed)

        curr_processed = file_list[last_processed_i + 1]
        curr_processed_i = last_processed_i + 1
    else:
        df = pd.DataFrame(columns = ['filename', 'direction', 'confidence'])
        df = df.set_index('filename')

        curr_processed_i = 0
        curr_processed = file_list[curr_processed_i]

    app.run(debug=True)
